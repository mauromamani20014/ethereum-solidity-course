import Link from 'next/link';
import factory from 'ethereum/factory';
import { Card, Button } from 'semantic-ui-react';

const CampaignPage = ({ campaigns = [] }) => {
  const renderCampaigns = () => {
    const items = campaigns.map((campaign) => ({
      header: campaign,
      description: (
        <Link href={`/campaigns/${campaign}`}>
          <a>View campaign</a>
        </Link>
      ),
      fluid: true,
    }));

    return <Card.Group items={items} />;
  };

  return (
    <>
      <h1>Home Page</h1>
      <h3>Open Campaigns</h3>

      <Link href='/campaigns/new'>
        <a>
          <Button
            floated='right'
            content='Create Campaign'
            icon='add circle'
            primary
          />
        </a>
      </Link>

      {renderCampaigns()}
    </>
  );
};

export const getServerSideProps = async () => {
  let campaigns = [];
  try {
    campaigns = await factory.methods.getDeployedCampaigns().call();
  } catch (e) {
    console.error('Failed to fetch campaigns!!');
    console.log(e);
  } finally {
    return {
      props: { campaigns },
    };
  }
};

export default CampaignPage;
