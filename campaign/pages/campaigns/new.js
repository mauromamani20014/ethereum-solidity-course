import { useState } from 'react';
import { useRouter } from 'next/router';
import { Button, Form, Input, Message } from 'semantic-ui-react';

import factory from 'ethereum/factory';
import web3 from 'ethereum/web3';

const NewPage = () => {
  const [minContribution, setMinContribution] = useState('');
  const [errorMsg, setErrorMsg] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const router = useRouter();

  const handleSubmit = async (ev) => {
    ev.preventDefault();

    setIsLoading(true);
    setErrorMsg('');

    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods.createCampaign(minContribution).send({
        from: accounts[0],
      });

      router.push('/');
    } catch (e) {
      console.log(e);
      setErrorMsg(e.message);
    }
    setIsLoading(false);
  };

  return (
    <>
      <h3>Create a Campaign</h3>

      <Form onSubmit={handleSubmit} error={!!errorMsg}>
        <Form.Field>
          <label>Minimum Contribution</label>
          <Input
            label='wei'
            labelPosition='right'
            value={minContribution}
            onChange={({ target }) => setMinContribution(target.value)}
          />
        </Form.Field>

        <Message error header='Oops!' content={errorMsg} />

        <Button loading={isLoading} content='Create!' primary />
      </Form>
    </>
  );
};

export default NewPage;
