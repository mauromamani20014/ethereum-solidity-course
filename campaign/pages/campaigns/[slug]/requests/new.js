import { Form, Button, Message, Input } from 'semantic-ui-react';
import web3 from 'ethereum/web3';
import getCampaignInstance from 'ethereum/campaign';
import Link from 'next/link';
import { useForm } from 'src/hooks/useForm';
import { useState } from 'react';

const NewRequestPage = ({ address }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [message, setMessage] = useState('');

  const { values, handleInputChange, resetForm } = useForm({
    description: '',
    value: '',
    recipient: '',
  });

  const handleSubmit = async (ev) => {
    ev.preventDefault();

    // enable spinner & clear message
    setIsLoading(true);
    setMessage('');

    const { description, value, recipient } = values;
    const campaignInstance = getCampaignInstance(address);
    try {
      const [account] = await web3.eth.getAccounts();

      await campaignInstance.methods
        .createRequest(description, web3.utils.toWei(value, 'ether'), recipient)
        .send({
          from: account,
        });

      resetForm();
    } catch (err) {
      setMessage(err.message);
      console.log(err);
    }
    // disable spinner
    setIsLoading(false);
  };

  return (
    <>
      <Link href={`/campaigns/${address}/requests`}>
        <a>Back</a>
      </Link>
      <h1>Create a Request</h1>
      <Form onSubmit={handleSubmit} error={!!message}>
        <Form.Field>
          <label>Description</label>
          <Input
            name='description'
            value={values.description}
            onChange={handleInputChange}
          />
        </Form.Field>

        <Form.Field>
          <label>Value in Ether</label>
          <Input
            name='value'
            value={values.value}
            onChange={handleInputChange}
          />
        </Form.Field>

        <Form.Field>
          <label>Recipient</label>
          <Input
            name='recipient'
            value={values.recipient}
            onChange={handleInputChange}
          />
        </Form.Field>

        <Message error header='Oops!' content={message} />

        <Button loading={isLoading} primary>
          Create
        </Button>
      </Form>
    </>
  );
};

export const getServerSideProps = async (context) => {
  const address = context.params.slug;

  return {
    props: { address },
  };
};

export default NewRequestPage;
