import { Button, Table } from 'semantic-ui-react';
import Link from 'next/link';
import getCampaignInstance from 'ethereum/campaign';
import { RequestRow } from 'src/components/RequestRow';

// display a list of requests
const RequestsPage = ({ address, requests, approversCount }) => {
  const { Header, Row, HeaderCell, Body } = Table;
  const requestsParsed = [...JSON.parse(requests)];

  const renderRow = () => {
    return requestsParsed.map((request, idx) => (
      <RequestRow
        request={request}
        idx={idx}
        key={idx}
        address={address}
        approversCount={approversCount}
      />
    ));
  };

  return (
    <div>
      <h3>Request List</h3>
      <Table>
        <Header>
          <Row>
            <HeaderCell>ID</HeaderCell>
            <HeaderCell>Description</HeaderCell>
            <HeaderCell>Amount</HeaderCell>
            <HeaderCell>Recipient</HeaderCell>
            <HeaderCell>Approval count</HeaderCell>
            <HeaderCell>Approve</HeaderCell>
            <HeaderCell>Finalize</HeaderCell>
          </Row>
        </Header>

        <Body>{renderRow()}</Body>
      </Table>

      <Link href={`/campaigns/${address}/requests/new`}>
        <a>
          <Button primary>New Request</Button>
        </a>
      </Link>
    </div>
  );
};

export const getServerSideProps = async (context) => {
  const address = context.params.slug;
  const campaignInstance = getCampaignInstance(address);

  // con la cantidad de request obtenemos uno a uno los request de la blockchain
  const requestsCount = await campaignInstance.methods
    .getRequestsCount()
    .call();
  const requests = await Promise.all(
    Array(parseInt(requestsCount))
      .fill()
      .map((_el, idx) => campaignInstance.methods.requests(idx).call())
  );
  const approversCount = await campaignInstance.methods.approversCount().call();

  return {
    props: {
      address,
      requests: JSON.stringify(requests),
      requestsCount,
      approversCount,
    },
  };
};

export default RequestsPage;
