import { Button, Card, Grid } from 'semantic-ui-react';
import Link from 'next/link';

import { ContributeForm } from 'src/components/ContributeForm';

import getCampaignInstance from 'ethereum/campaign';
import web3 from 'ethereum/web3';

const CampaignPage = ({ sumary }) => {
  const renderCards = () => {
    const items = [
      {
        header: sumary.manager,
        meta: 'Address of manager',
        description:
          'The manager created this campaign and can create requests',
        style: { overflowWrap: 'break-word' },
      },
      {
        header: sumary.minimumContribution,
        meta: 'Minimum Contribution',
        description:
          'You must contribute at least this much wei to become an approver',
      },
      {
        header: sumary.numRequests,
        meta: 'Number of Requests',
        description: 'A request tries to withdraw money from the campaign',
      },
      {
        header: sumary.approversCount,
        meta: 'Numbers of Approvers',
        description: 'Number of people who already donated to this campaign',
      },
      {
        header: web3.utils.fromWei(sumary.balance, 'ether'),
        meta: 'Campaign Balance (eth)',
        description:
          'The balance is how much money this campaign has left to spend',
      },
    ];

    return <Card.Group items={items} />;
  };

  return (
    <div>
      <h1>Campaign</h1>
      <Grid>
        <Grid.Row>
          <Grid.Column width={10}>{renderCards()}</Grid.Column>
          <Grid.Column width={6}>
            <ContributeForm address={sumary.address} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={6}>
            <Link href={`/campaigns/${sumary.address}/requests`}>
              <a>
                <Button primary>View Requests</Button>
              </a>
            </Link>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export const getServerSideProps = async (context) => {
  const address = context.params.slug;
  const campaignInstance = getCampaignInstance(address);
  const campaignSummary = await campaignInstance.methods.getSummary().call();

  const sumary = {
    address,
    minimumContribution: campaignSummary[0],
    balance: campaignSummary[1],
    numRequests: campaignSummary[2],
    approversCount: campaignSummary[3],
    manager: campaignSummary[4],
  };

  return {
    props: { sumary },
  };
};

export default CampaignPage;
