import { Header } from './Header';
import { Container } from 'semantic-ui-react';

export const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <Container>{children}</Container>
    </>
  );
};
