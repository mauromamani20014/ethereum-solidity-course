import web3 from 'ethereum/web3';
import getCampaignInstance from 'ethereum/campaign';
import { Table, Button } from 'semantic-ui-react';
import { useRouter } from 'next/router';

export const RequestRow = ({ request, idx, approversCount, address }) => {
  const router = useRouter();
  const { Row, Cell } = Table;

  const handleApproveRequest = async () => {
    const campaignInstance = getCampaignInstance(address);
    const [account] = await web3.eth.getAccounts();

    await campaignInstance.methods.approveRequest(idx).send({
      from: account,
    });

    router.reload(window.location.pathname);
  };

  const handleFinalizeRequest = async () => {
    const campaignInstance = getCampaignInstance(address);
    const [account] = await web3.eth.getAccounts();

    await campaignInstance.methods.finalizeRequest(idx).send({
      from: account,
    });

    router.reload(window.location.pathname);
  };

  const isReadyToFinalize = request.approvalCount > approversCount / 2;

  return (
    <Row
      disabled={request.complete}
      positive={isReadyToFinalize && !request.complete}
    >
      <Cell>{idx}</Cell>
      <Cell>{request.description}</Cell>
      <Cell>
        {web3.utils.fromWei(request.value, 'ether')} <strong>(ETH)</strong>
      </Cell>
      <Cell>{request.recipient}</Cell>
      <Cell>
        {request.approvalCount} / {approversCount}
      </Cell>
      <Cell>
        <Button
          disabled={request.complete}
          color='green'
          basic
          onClick={handleApproveRequest}
        >
          Approve
        </Button>
      </Cell>
      <Cell>
        <Button
          disabled={request.complete || !isReadyToFinalize}
          color='teal'
          basic
          onClick={handleFinalizeRequest}
        >
          {request.complete ? 'Finalized' : 'Finalize'}
        </Button>
      </Cell>
    </Row>
  );
};
