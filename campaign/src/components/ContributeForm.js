import { useRouter } from 'next/router';
import { useState } from 'react';
import { Form, Input, Button } from 'semantic-ui-react';
import getCampaignInstance from '../../ethereum/campaign';
import web3 from '../../ethereum/web3';

export const ContributeForm = ({ address }) => {
  const router = useRouter();
  const [value, setValue] = useState(0);

  const handleSubmit = async (ev) => {
    ev.preventDefault();
    const campaignInstance = getCampaignInstance(address);

    try {
      const accounts = await web3.eth.getAccounts();
      await campaignInstance.methods.contribute().send({
        from: accounts[0],
        value: web3.utils.toWei(value, 'ether'),
      });
      router.reload();
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Field>
        <label>Amount to Contribute</label>
        <Input
          label='ether'
          labelPosition='right'
          onChange={({ target }) => setValue(target.value)}
        />
      </Form.Field>
      <Button primary>Contribute</Button>
    </Form>
  );
};
