import Web3 from 'web3';

let web3;
const url = 'https://rinkeby.infura.io/v3/b1e343bb8e7c450c89a0ec01d1361e49';

if (typeof window !== 'undefined' && typeof window.ethereum !== 'undefined') {
  web3 = new Web3(window.ethereum);
  window.ethereum.request({ method: 'eth_requestAccounts' });
} else {
  const provider = new Web3.providers.HttpProvider(url);
  web3 = new Web3(provider);
}

export default web3;
