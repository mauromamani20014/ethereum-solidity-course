import web3 from './web3';
import CampaignFactory from './build/CampaignFactory.json';

const address = '0x3808a919448177f50C9DD27Cf345C06f1Ceb5fE4';

const instance = new web3.eth.Contract(CampaignFactory.abi, address);

export default instance;
