const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { interface, bytecode } = require('./compile');

// Esta funcion se encargarpa de generar el provider, y hacer el deploy a la red rinkeby
const provider = new HDWalletProvider(
  'burst lake history party reform drop pair critic wagon miss unhappy genius',
  'https://rinkeby.infura.io/v3/b1e343bb8e7c450c89a0ec01d1361e49'
);
const web3 = new Web3(provider);
// address 0xE67D9545AE536FD3C5E17a41D466aEce4B9a22fe
const deploy = async () => {
  const accounts = await web3.eth.getAccounts(); // cuentas para tests
  console.log('Attempting to deploy from account: ', accounts[0]);
  const result = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: '0x' + bytecode, arguments: ['Hi there!'] }) // add 0x bytecode
    .send({ from: accounts[0] }); // remove 'gas't

  console.log('Contract deployed to: ', result.options.address);
};

deploy();
