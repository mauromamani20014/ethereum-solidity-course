const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const { interface, bytecode } = require('../compile');

const web3 = new Web3(ganache.provider());
// https://rinkeby.infura.io/v3/b1e343bb8e7c450c89a0ec01d1361e49
let fetchedAccounts;
let inbox;

beforeEach(async () => {
  // Get a list of all acounts
  fetchedAccounts = await web3.eth.getAccounts(); // cuentas para tests

  //  Use one of those acc to deploy the contract
  inbox = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({
      data: bytecode,
      arguments: ['Hi there'], // es lo que el constructor necesita
    })
    .send({ from: fetchedAccounts[0], gas: '1000000' });
});

describe('Contract test', () => {
  it('Should deploys a contract', () => {
    // cuando se hace un deploy, address toma la direccion de donde se esta desplegando el contrato
    assert.ok(inbox.options.address);
  });

  it('should has a default message', async () => {
    // obteniendo el valor de los atributos del contrato
    const message = await inbox.methods.message().call();
    assert.equal(message, 'Hi there');
  });

  it('Should change the message', async () => {
    // haciendo una transaccion
    await inbox.methods.setMessage('bye').send({ from: fetchedAccounts[0] });

    const message = await inbox.methods.message().call();
    assert.equal(message, 'bye');
  });
});
