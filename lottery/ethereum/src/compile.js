const path = require('path');
const fs = require('fs');
const solc = require('solc');

const lotteryPath = path.resolve(
  __dirname,
  'contracts',
  'lottery.contract.sol'
);
const source = fs.readFileSync(lotteryPath, 'utf8');

const input = {
  language: 'Solidity',
  sources: {
    'lottery.contract.sol': {
      content: source,
    },
  },
  settings: {
    metadata: {
      useLiteralContent: true,
    },
    outputSelection: {
      '*': {
        '*': ['*'],
      },
    },
  },
};

const output = JSON.parse(solc.compile(JSON.stringify(input)));
module.exports = output.contracts['lottery.contract.sol'].Lottery;
