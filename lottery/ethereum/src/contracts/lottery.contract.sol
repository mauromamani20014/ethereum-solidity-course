// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.5.0 <0.9.0;
  
contract Lottery {
  address private manager;
  address payable[] private players;
  
  constructor() {
    manager = msg.sender;
  }
  
  function enter() public payable {
    require(msg.value > .01 ether, "A minimum payment of .01 ether must be sent to enter the lottery");
    players.push(payable(msg.sender));
  }
  
  function pickWinner() public onlyOwner {
    uint256 idx = random() % players.length;
    address contractAddress = address(this);
    
    players[idx].transfer(contractAddress.balance);
    players = new address payable[](0);
  }
  
  function random() private view returns (uint256) {
    return
      uint256(
        keccak256(
          abi.encodePacked(block.difficulty, block.number, players)
        )
      );
  }
  
  // Getters
  function getManager() public view returns (address) {
    return manager;
  }
  
  function getPlayers() public view returns (address payable[] memory) {
    return players;
  }
  
  // modifiers
  modifier onlyOwner() {
    require(msg.sender == manager, "Only owner can call this function.");
    _;
  }
}
