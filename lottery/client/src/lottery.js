import web3 from './web3';

const abi = [
  {
    inputs: [],
    stateMutability: 'nonpayable',
    type: 'constructor',
    constant: undefined,
    payable: undefined,
    signature: 'constructor',
  },
  {
    inputs: [],
    name: 'enter',
    outputs: [],
    stateMutability: 'payable',
    type: 'function',
    constant: undefined,
    payable: true,
    signature: '0xe97dcb62',
  },
  {
    inputs: [],
    name: 'getManager',
    outputs: [[Object]],
    stateMutability: 'view',
    type: 'function',
    constant: true,
    payable: undefined,
    signature: '0xd5009584',
  },
  {
    inputs: [],
    name: 'getPlayers',
    outputs: [[Object]],
    stateMutability: 'view',
    type: 'function',
    constant: true,
    payable: undefined,
    signature: '0x8b5b9ccc',
  },
  {
    inputs: [],
    name: 'pickWinner',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
    constant: undefined,
    payable: undefined,
    signature: '0x5d495aea',
  },
];
const address = '0xdb053947A2637301870C92A8A36a7C0D6c81c91a';

export default new web3.eth.Contract(abi, address);
