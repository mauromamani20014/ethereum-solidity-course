import { useState, useEffect } from 'react';
import web3 from './web3';
import lottery from './lottery';

const App = () => {
  const [manager, setManager] = useState('');
  const [balance, setBalance] = useState('');
  const [value, setValue] = useState('');
  const [players, setPlayers] = useState([]);

  const [message, setMessage] = useState('');

  useEffect(() => {
    const getManager = async () => {
      await window.ethereum.enable();
      try {
        const manager = await lottery.methods.getManager().call();
        const players = await lottery.methods.getPlayers().call();
        const balance = await web3.eth.getBalance(lottery.options.address);

        setManager(manager);
        setPlayers(players);
        setBalance(balance);
      } catch (e) {
        console.log(e);
      }
    };
    getManager();
  }, []);

  const handleSubmit = async (ev) => {
    ev.preventDefault();
    try {
      const accounts = await web3.eth.getAccounts();
      console.log(accounts);
      console.log(players);

      setMessage('Waiting on transaction success...');

      await lottery.methods.enter().send({
        from: accounts[0],
        value: web3.utils.toWei(value, 'ether'),
      });

      setMessage('You have been entered');
    } catch (e) {
      console.log(e);
      setMessage('Something went wrong! :(');
    }
  };

  const handlePickWinner = async () => {
    try {
      const accounts = await web3.eth.getAccounts();

      setMessage('Waiting on transaction success...');

      await lottery.methods.pickWinner().send({
        from: accounts[0],
      });

      setMessage('A Winner has been piked');
    } catch (e) {
      setMessage('Something went wrong! :(');
      console.log(e);
    }
  };

  return (
    <div>
      <h1>Lottery Game ETH</h1>
      <hr />

      <h3>Manager of game: {manager}</h3>
      <p>
        There are currently {players.length} players entered, competing to win
        some amount of ETH({web3.utils.fromWei(balance, 'ether')})
      </p>
      <hr />

      <form onSubmit={handleSubmit}>
        <h4>Want to try your luck?</h4>
        <div>
          <label>Amount of ETH to enter</label>
          <input
            onChange={({ target }) => setValue(target.value)}
            value={value}
          />
        </div>

        <button type="submit">Enter</button>
      </form>

      <hr />
      <h2>{message}</h2>

      <hr />
      <h4>Ready to pick a Winner?</h4>
      <button type="button" onClick={handlePickWinner}>
        Pick A Winner
      </button>
    </div>
  );
};

export default App;
