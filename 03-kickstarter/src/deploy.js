const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledFactory = require('./build/CampaignFactory.json');

// Esta funcion se encargará de generar el provider, y hacer el deploy a la red rinkeby
const provider = new HDWalletProvider(
  'burst lake history party reform drop pair critic wagon miss unhappy genius',
  'https://rinkeby.infura.io/v3/b1e343bb8e7c450c89a0ec01d1361e49'
);
const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts(); // cuentas para tests
  console.log('Attempting to deploy from account: ', accounts[0]);

  const result = await new web3.eth.Contract(
    JSON.parse(compiledFactory.interface)
  )
    .deploy({ data: '0x' + compiledFactory.bytecode }) // add 0x bytecode
    .send({ from: accounts[0] }); // remove 'gas'

  console.log('Contract deployed to: ', result.options.address);
};

//  contract address : 0x9b99a2f2BD890500CbBb44EF7A7E8FCe58D476E9
deploy();
