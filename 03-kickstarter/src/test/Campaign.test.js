const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const compiledFactory = require('../build/CampaignFactory.json');
const compiledCampaign = require('../build/Campaign.json');

const web3 = new Web3(ganache.provider());

let factory;
let campaign;
let campaignAddress;
let accounts = [];

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  // create campaign factory contract
  factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
    .deploy({ data: '0x' + compiledFactory.bytecode })
    .send({ from: accounts[0], gas: '1000000' });

  // create campaign contract
  const minimumContribution = '100'; // amount of wei
  await factory.methods.createCampaign(minimumContribution).send({
    from: accounts[0],
    gas: '1000000',
  });

  // get address of created contract
  [campaignAddress] = await factory.methods.getDeployedCampaigns().call();

  // get instance of the contract
  campaign = await new web3.eth.Contract(
    JSON.parse(compiledCampaign.interface),
    campaignAddress
  );
});

describe('Campaign contract', () => {
  it('Should be deployed: factory & campaign contracts', () => {
    assert.ok(factory.options.address);
    assert.ok(campaign.options.address);
  });

  it('marks calleras the campaign manager', async () => {
    const manager = await campaign.methods.manager().call();
    assert.equal(accounts[0], manager);
  });

  it('allows people to contribute money and marks them as approvers', async () => {
    await campaign.methods.contribute().send({
      value: '200',
      from: accounts[1],
    });

    // if a contributor has been marked as approver, then returns true
    const isContributor = await campaign.methods.approvers(accounts[1]).call();
    assert(isContributor);
  });

  it('should returns an error if the minimumContribution is lt = 100 wei', async () => {
    try {
      await campaign.methods.contribute().send({
        value: '10',
        from: accounts[1],
      });
      assert(false);
    } catch (e) {
      assert(e);
    }
  });

  it('should allows a manager to make a payment request', async () => {
    await campaign.methods
      .createRequest('buy batteries', '10000', accounts[1])
      .send({
        from: accounts[0],
        gas: '1000000',
      });

    const request = await campaign.methods.requests(0).call();

    assert.equal('buy batteries', request.description);
    assert.equal('10000', request.value);
    assert.equal(accounts[1], request.recipent);
  });

  it('should processes requests', async () => {
    await campaign.methods.contribute().send({
      from: accounts[0],
      value: web3.utils.toWei('10', 'ether'),
    });

    let balance = await web3.eth.getBalance(accounts[1]);
    balance = web3.utils.fromWei(balance, 'ether');
    balance = parseFloat(balance);

    await campaign.methods
      .createRequest(
        'buy batteries',
        web3.utils.toWei('5', 'ether'),
        accounts[1]
      )
      .send({
        from: accounts[0],
        gas: '1000000',
      });

    await campaign.methods.approveRequest(0).send({
      from: accounts[0],
      gas: '1000000',
    });

    await campaign.methods.finalizeRequest(0).send({
      from: accounts[0],
      gas: '1000000',
    });

    let newBalance = await web3.eth.getBalance(accounts[1]);
    newBalance = web3.utils.fromWei(newBalance, 'ether');
    newBalance = parseFloat(newBalance);

    console.log({ newBalance, balance });
    assert(newBalance > balance);
  });
});
