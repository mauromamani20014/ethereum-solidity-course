pragma solidity ^0.4.17;

contract CampaignFactory {
  address[] public deployedCampaigns;
  
  function createCampaign(uint minimumContribution) public {
    // se crea el contrato y se hace el deploy a la blockchain
    address campaignAddress = new Campaign(minimumContribution, msg.sender);
    deployedCampaigns.push(campaignAddress);
  }
  
  function getDeployedCampaigns() public view returns (address[]) {
    return deployedCampaigns;
  }
}

contract Campaign {
  struct Request {
    string                   description;
    uint                     value;
    address                  recipent;
    bool                     complete;
    uint                     approvalCount;
    mapping(address => bool) approvals;
  } 
  
  Request[] public requests;
  address public manager;
  uint public minimumContribution;
  mapping(address => bool) public approvers;
  uint public approversCount;
  
  //address[] public approvers; cambiamos a un map por temas de rendimiento y costo 
  
  function Campaign(uint _minimumContribution, address creator) public {
    manager = creator;
    minimumContribution = _minimumContribution;
  }
  
  // Send money to contract
  function contribute() public payable {
    // weis
    require(msg.value >= minimumContribution);
    // agragando un contribute al map
    approvers[msg.sender] = true;
    approversCount++;
  }
  
  // Create a request
  function createRequest(string desc, uint value, address recipent) public restricted {
    Request memory newRequest = Request({
       description: desc,
       value: value,
       recipent: recipent,
       complete: false,
       approvalCount: 0
    });
    requests.push(newRequest);
  }
  
  function approveRequest(uint idx) public {
    Request storage request = requests[idx]; // usamos storage para hacer un puntero al arreglo original
    
    // verificando que la persona haya donado 
    require(approvers[msg.sender]);
    // verificando que la persona no haya aprobado el request
    require(!request.approvals[msg.sender]);
    
    request.approvals[msg.sender] = true;
    request.approvalCount++;
  }
  
  function finalizeRequest(uint idx) public restricted {
    Request storage request = requests[idx]; // usamos storage para hacer un puntero al arreglo original
    
    require(request.approvalCount > (approversCount / 2)); // verificando que los votos positivos sean mayores a la mitad de los participantes
    require(!request.complete); // que no este completa
    
    request.recipent.transfer(request.value); // transferimos el dinero
    request.complete = true;
  } 

  // Modifiers
  modifier restricted() {
    require(msg.sender == manager);
    _;
  }
}
