const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const { interface, bytecode } = require('../compile');

const web3 = new Web3(ganache.provider());

let lottery;
let accounts;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  lottery = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: '0x' + bytecode })
    .send({ from: accounts[0], gas: '1000000' });
});

describe('Lottery contract', () => {
  it('Should deploys a contract', () => {
    assert.ok(lottery.options.address);
  });

  it('Should be equal: manager and the contract creator', async () => {
    const manager = await lottery.methods.getManager().call({
      from: accounts[0],
    });

    assert.equal(manager, accounts[0]);
  });

  it('Should allows one acc to enter', async () => {
    await lottery.methods.enter().send({
      from: accounts[1],
      value: web3.utils.toWei('0.02', 'ether'),
    });

    const players = await lottery.methods.getPlayers().call({
      from: accounts[0],
    });

    assert.equal(accounts[1], players[0]);
    assert.equal(1, players.length);
  });

  it('Should throw an error if manager enter to the lottery', async () => {
    try {
      await lottery.methods.enter().send({
        from: accounts[0],
        value: web3.utils.toWei('0.02', 'ether'),
      });
      assert(false);
    } catch (e) {
      assert(e);
    }
  });

  it('Should throw an error if a no minimum amount has been sent', async () => {
    try {
      await lottery.methods.enter().send({
        from: accounts[0],
        value: 100,
      });
      assert(false);
    } catch (e) {
      assert(e);
    }
  });

  it('Should allows multiples accounts to enter', async () => {
    await lottery.methods.enter().send({
      from: accounts[1],
      value: web3.utils.toWei('0.02', 'ether'),
    });
    await lottery.methods.enter().send({
      from: accounts[2],
      value: web3.utils.toWei('0.02', 'ether'),
    });
    await lottery.methods.enter().send({
      from: accounts[3],
      value: web3.utils.toWei('0.02', 'ether'),
    });

    const players = await lottery.methods.getPlayers().call({
      from: accounts[0],
    });

    assert.equal(accounts[1], players[0]);
    assert.equal(accounts[2], players[1]);
    assert.equal(accounts[3], players[2]);
    assert.equal(3, players.length);
  });

  it('Should throw an error if manager not call pickWinner', async () => {
    try {
      await lottery.methods.pickWinner().send({
        from: accounts[2],
      });
      assert(false);
    } catch (e) {
      assert(e);
    }
  });

  it('Should sends money to the winner and resets the players array', async () => {
    // enter a player
    await lottery.methods.enter().send({
      from: accounts[1],
      value: web3.utils.toWei('2', 'ether'),
    });

    const initialBalance = await web3.eth.getBalance(accounts[1]);
    await lottery.methods.pickWinner().send({
      from: accounts[0],
    });

    const finalBalance = await web3.eth.getBalance(accounts[1]);

    const difference = finalBalance - initialBalance;
    const players = await lottery.methods.getPlayers().call({
      from: accounts[0],
    });

    assert(difference > web3.utils.toWei('1.8', 'ether'));
    assert.equal(0, players.length);
  });
});
