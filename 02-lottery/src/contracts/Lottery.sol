pragma solidity ^0.4.17;

contract Lottery {
    address private manager;
    address[] private players;
    
    // constructor
    function Lottery() public {
        // Getting address who made the transaction with the global variable "msg"
        manager = msg.sender;
    }
    
    function getManager() public view returns (address) {
        return manager;
    }
    
    function getPlayers() public view returns (address[]) {
        return players;
    }
    
    // when a function expects an ether, we need to put the word "payable"
    function enter() public payable {
        // es usado para validacion
        require(getManager() != msg.sender);
        require(msg.value > .01 ether);

        // add new address who sent the transaction to players array  
        players.push(msg.sender);
    }
    
    function pickWinner() public restricted {
        // only the manager can execute this function with the modifier "restricted"
        
        uint idx = random() % players.length;
        // this.balance : all the money entered into contract
        // we send the money to the chosen address
        players[idx].transfer(this.balance); // address of player - transfer function sends weis to the chosen address
        
        // reset the contract
        players = new address[](0);
    }
    
    // solidity cannot generate random numbers, instead we create a function that generate "random" numbers
    function random() private view returns (uint) {
        return uint(keccak256(block.difficulty, now, players)); // returns a hash, then convert into uint
    }
    
    // modifier functions
    modifier restricted() {
        require(getManager() == msg.sender);
        _;
    }
}
