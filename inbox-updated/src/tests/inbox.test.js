const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const { abi, evm } = require('../compile');

const provider = ganache.provider();
const web3 = new Web3(provider);

const message = 'Hello world';
let accounts = [];
let inbox;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  inbox = await new web3.eth.Contract(abi)
    .deploy({ data: '0x' + evm.bytecode.object, arguments: [message] })
    .send({ from: accounts[0], gas: '1000000' });
});

describe('Inbox contract tests', () => {
  it('Should be defined', () => {
    assert.ok(inbox.options.address);
  });

  it('Should be equal between the provided value and the getMessage method', async () => {
    const msg = await inbox.methods.getMessage().call();
    assert.strictEqual(msg, message);
  });

  it('Should change the value', async () => {
    const newMessage = 'Bye!';
    await inbox.methods.setMessage(newMessage).send({ from: accounts[0] });

    const msg = await inbox.methods.getMessage().call();
    assert.strictEqual(msg, newMessage);
  });
});
